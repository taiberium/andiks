package com.getjavajob.akhmetov.centTask;

/**
 * Created by Taiberium on 27.10.2015.
 */
public class MoneyCounter {

    final static int[] coinsValArray = new int[]{1, 5, 10, 25, 50};

    public static int countChanges(int amount) {
        return countChanges(amount, coinsValArray.length - 1);
    }

    private static int countChanges(int amount, int coinArraySize) {
        if (amount < 0 || coinArraySize < 0) return 0;
        if (amount == 0 || coinArraySize == 0) return 1;
        return countChanges(amount, coinArraySize - 1) + countChanges(amount - coinsValArray[coinArraySize], coinArraySize);
    }

}
