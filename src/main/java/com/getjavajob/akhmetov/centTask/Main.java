package com.getjavajob.akhmetov.centTask;

/**
 * Created by Taiberium on 27.10.2015.
 */
public class Main {

    public static void main(String[] args) {
        System.out.println("test");
        System.out.println("55 cents:" + (MoneyCounter.countChanges(55) == 62));
        System.out.println("2 cents:" + (MoneyCounter.countChanges(2) == 1));
        System.out.println("5 cents:" + (MoneyCounter.countChanges(5) == 2));
        System.out.println("11 cents:" + (MoneyCounter.countChanges(11) == 4));
        System.out.println("25 cents:" + (MoneyCounter.countChanges(25) == 13));
    }

}
