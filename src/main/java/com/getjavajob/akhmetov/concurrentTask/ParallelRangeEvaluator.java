package com.getjavajob.akhmetov.concurrentTask;

import com.getjavajob.akhmetov.concurrentTask.task.RangeTask;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Taiberium on 27.10.2015.
 */
public class ParallelRangeEvaluator implements Evaluator {

    private static final int ARRAY_RANGE = 1024;

    @Override
    public int[] evaluate(int[] data, int power) {
        int threadsNum = Runtime.getRuntime().availableProcessors() + 1;
        ExecutorService executor = Executors.newFixedThreadPool(threadsNum);
        int[] resultData = new int[data.length];
        int iterations = data.length / ARRAY_RANGE + 1;
        int rest = data.length % ARRAY_RANGE;
        CountDownLatch counter = new CountDownLatch(iterations);
        for (int z = 0; z < iterations; z++) {
            int firstIndex = z * ARRAY_RANGE;
            int lastIndex = z == iterations - 1 ? z * ARRAY_RANGE + rest : (z + 1) * ARRAY_RANGE;
            executor.execute(new RangeTask(power, firstIndex, lastIndex, resultData, data, counter));
        }
        executor.shutdown();
        try {
            counter.await();
        } catch (InterruptedException e) {
            throw new RuntimeException("ParallelRangeEvaluator interrupted", e);
        }
        return resultData;
    }
}

