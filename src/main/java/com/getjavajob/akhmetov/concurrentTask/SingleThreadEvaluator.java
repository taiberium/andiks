package com.getjavajob.akhmetov.concurrentTask;

import com.getjavajob.akhmetov.concurrentTask.extlib.ExtLib;

/**
 * Created by Taiberium on 27.10.2015.
 */
public class SingleThreadEvaluator implements Evaluator {
    @Override
    public int[] evaluate(int[] data, int power) {
        int[] resultData = new int[data.length];
        for (int i = 0; i < data.length; i++) {
            resultData[i] = ExtLib.eval(data[i], power);
        }
        return resultData;
    }
}
