package com.getjavajob.akhmetov.concurrentTask;

import com.getjavajob.akhmetov.concurrentTask.extlib.ExtLib;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Taiberium on 27.10.2015.
 */
public class ParallelSerialEvaluator implements Evaluator {

    private int[] resultData;
    private AtomicInteger arrayIndex = new AtomicInteger();

    @Override
    public int[] evaluate(final int[] data, final int power) {
        int coreNum = Runtime.getRuntime().availableProcessors() + 1;
        ExecutorService executor = Executors.newFixedThreadPool(coreNum);
        final CountDownLatch counter = new CountDownLatch(coreNum);
        resultData = new int[data.length];
        Runnable task = new Runnable() {
            @Override
            public void run() {
                while (arrayIndex.get() < resultData.length) {
                         /*Закешированный индекс для каждого отдельного потока, он гарантирует, что изменения
                        * внесенные в arrayIndex другими потоками не повлияют на работу нынешнего, пока он не возьмет
                        * другой индекс из arrayIndex*/
                    int threadIndex = arrayIndex.getAndIncrement();
                    if (threadIndex < resultData.length) {
                        resultData[threadIndex] = ExtLib.eval(data[threadIndex], power);
                    }
                }
                counter.countDown();
            }
        };
        for (int i = 0; i < coreNum; i++) {
            executor.execute(task);
        }
        executor.shutdown();
        try {
            counter.await();
            arrayIndex.set(0);
            return resultData;
        } catch (InterruptedException e) {
            throw new RuntimeException("ParallelSerialEvaluator interrupted", e);
        }
    }
}