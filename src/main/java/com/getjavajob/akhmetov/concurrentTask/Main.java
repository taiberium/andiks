package com.getjavajob.akhmetov.concurrentTask;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by Taiberium on 27.10.2015.
 */
public class Main {

    private static final int RANDOM_RANGE = 10;
    private static final int NUMBER_OF_ITERATIONS = 100;

    public static void main(String[] args) {
        final int arraySize = 3200000;
        final int power = 3;
        final int[] data = makeRandomArray(arraySize);
        final SingleThreadEvaluator singleThreadEvaluator = new SingleThreadEvaluator();
        final ParallelSerialEvaluator parallelSerialEvaluator = new ParallelSerialEvaluator();
        final ParallelRangeEvaluator parallelRangeEvaluator = new ParallelRangeEvaluator();

        //speed test
        System.out.println("SingleThreadEvaluator, speed test:");
        timeTest(data, singleThreadEvaluator, power);
        System.out.println("ParallelSerialEvaluator, speed test:");
        timeTest(data, parallelSerialEvaluator, power);
        System.out.println("ParallelRangeEvaluator, speed test:");
        timeTest(data, parallelRangeEvaluator, power);

        //test result equality
        int[] exampleArray = singleThreadEvaluator.evaluate(data, power);
        int[] parallelSerialArray = parallelSerialEvaluator.evaluate(data, power);
        int[] parallelRangeArray = parallelRangeEvaluator.evaluate(data, power);

        System.out.println("Arrays are equal: " + Arrays.equals(exampleArray, parallelSerialArray));
        System.out.println("Arrays are equal: " + Arrays.equals(exampleArray, parallelRangeArray));
    }

    private static void timeTest(int[] data, Evaluator evaluator, int power) {
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < NUMBER_OF_ITERATIONS; i++) {
            evaluator.evaluate(data, power);
        }
        long endTime = System.currentTimeMillis();
        System.out.println("Time:" + (endTime - startTime) + " ms");
    }

    private static int[] makeRandomArray(int arraySize) {
        int[] array = new int[arraySize];
        Random random = new Random();
        for (int i = 0; i < arraySize; i++) {
            array[i] = random.nextInt(RANDOM_RANGE);
        }
        return array;
    }
}
