package com.getjavajob.akhmetov.concurrentTask.task;

import com.getjavajob.akhmetov.concurrentTask.extlib.ExtLib;

import java.util.concurrent.CountDownLatch;

/**
 * Created by Taiberium on 28.10.2015.
 */
public class RangeTask implements Runnable {

    private int power;
    private int firstIndex;
    private int lastIndex;
    private int[] resultData;
    private int[] data;
    private CountDownLatch countDownLatch;

    public RangeTask(int power, int firstIndex, int lastIndex, int[] resultData, int[] data, CountDownLatch countDownLatch) {
        this.power = power;
        this.firstIndex = firstIndex;
        this.lastIndex = lastIndex;
        this.resultData = resultData;
        this.data = data;
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {
        for (int i = firstIndex; i < lastIndex; i++) {
            resultData[i] = ExtLib.eval(data[i], power);
        }
        countDownLatch.countDown();
    }
}
