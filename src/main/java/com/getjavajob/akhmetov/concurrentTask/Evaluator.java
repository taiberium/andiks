package com.getjavajob.akhmetov.concurrentTask;

/**
 * Created by Taiberium on 27.10.2015.
 */
interface Evaluator {
    int[] evaluate(int[] data, int power);
}
