package com.getjavajob.akhmetov.concurrentTask.extlib;

/**
 * Created by Taiberium on 27.10.2015.
 */

public class ExtLib {
    public static int eval(int number, int power) {
        return (int) Math.pow(number, power);
    }
}
